FROM pandoc/latex
RUN apk add make grep inkscape evince hunspell biber inotify-tools
RUN tlmgr update --self && tlmgr install titlesec chngcntr todonotes glossaries mfirstuc xfor datatool libertine dejavu
# COPY ./ /data/
